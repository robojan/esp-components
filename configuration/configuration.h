#pragma once

#include <array>
#include <vector>
#include <string>
#include <string_view>
#include <typemap.h>
#include <nvs.h>
#include <type_traits>
#include <optional>
#include <UserConfiguration.h>

class Configuration
{
    template <auto value, typename>
    static constexpr auto dependent_value = value;

public:
    Configuration();
    ~Configuration();
    Configuration(Configuration &&other) = default;
    Configuration(const Configuration &) = delete;

    template <ConfigId I>
    auto get_default() const
    {
        auto def         = ConfigDefaultMap.get<I>();
        using outputType = ConfigTypeMap::get_type<I>;
        if constexpr(std::is_enum_v<outputType>)
        {
            int  v;
            bool success = get_value(I, v);
            return success ? static_cast<outputType>(v) : def;
        }
        else if constexpr(std::is_same_v<outputType, std::string>)
        {
            std::string v;
            bool        success = get_value(I, v);
            return success ? v : def;
        }
        else if constexpr(std::is_same_v<outputType, bool>)
        {
            uint8_t v;
            bool    success = get_value(I, v);
            return success ? v != 0 : def;
        }
        else if constexpr(std::is_scalar_v<outputType>)
        {
            outputType v;
            bool       success = get_value(I, v);
            return success ? v : def;
        }
        else if constexpr(std::is_pod_v<outputType>)
        {
            using outputType = ConfigTypeMap::get_type<I>;
            outputType v;
            bool       success = get_value(I, &v, sizeof(outputType));
            return success ? static_cast<outputType>(v) : def;
        }
        else
        {
            static_assert(dependent_value<false, outputType>, "Type of setting is not supported");
        }
    }

    template <ConfigId I>
    auto get() const
    {
        using outputType = ConfigTypeMap::get_type<I>;
        if constexpr(std::is_enum_v<outputType>)
        {
            int  v;
            bool success = get_value(I, v);
            return success ? std::make_optional(static_cast<outputType>(v)) : std::nullopt;
        }
        else if constexpr(std::is_same_v<outputType, std::string>)
        {
            std::string v;
            bool        success = get_value(I, v);
            return success ? std::make_optional(v) : std::nullopt;
        }
        else if constexpr(std::is_same_v<outputType, bool>)
        {
            uint8_t v;
            bool    success = get_value(I, v);
            return success ? std::make_optional(v != 0) : std::nullopt;
        }
        else if constexpr(std::is_scalar_v<outputType>)
        {
            outputType v;
            bool       success = get_value(I, v);
            return success ? std::make_optional(v) : std::nullopt;
        }
        else if constexpr(std::is_pod_v<outputType>)
        {
            outputType v;
            bool       success = get_value(I, &v, sizeof(outputType));
            return success ? std::make_optional(v) : std::nullopt;
        }
        else
        {
            static_assert(dependent_value<false, outputType>, "Type of setting is not supported");
        }
    }

    template <ConfigId I>
    bool set(const ConfigTypeMap::get_type<I> &v)
    {
        using t = ConfigTypeMap::get_type<I>;
        if constexpr(std::is_enum_v<t>)
        {
            return set_value(I, static_cast<int>(v));
        }
        else if constexpr(std::is_same_v<t, bool>)
        {
            return set_value(I, static_cast<int8_t>(v));
        }
        else if constexpr(std::is_scalar_v<t>)
        {
            return set_value(I, v);
        }
        else if constexpr(std::is_convertible_v<t, std::string>)
        {
            if constexpr(std::is_same_v<t, std::string>)
            {
                return set_value(I, v);
            }
            else
            {
                return set_value(I, std::string(v));
            }
        }
        else if constexpr(std::is_pod_v<t>)
        {
            return set_value(I, &v, sizeof(t));
        }
        else
        {
            static_assert(dependent_value<false, t>, "Type of setting is not supported");
            return false;
        }
    }

private:
    bool get_value(ConfigId id, int8_t &out) const;
    bool get_value(ConfigId id, uint8_t &out) const;
    bool get_value(ConfigId id, int16_t &out) const;
    bool get_value(ConfigId id, uint16_t &out) const;
    bool get_value(ConfigId id, int32_t &out) const;
    bool get_value(ConfigId id, uint32_t &out) const;
    bool get_value(ConfigId id, int64_t &out) const;
    bool get_value(ConfigId id, uint64_t &out) const;
    bool get_value(ConfigId id, std::string &out) const;
    bool get_value(ConfigId id, std::vector<std::byte> &out) const;
    bool get_value(ConfigId id, void *out, size_t size) const;
    bool set_value(ConfigId id, int8_t v);
    bool set_value(ConfigId id, uint8_t v);
    bool set_value(ConfigId id, int16_t v);
    bool set_value(ConfigId id, uint16_t v);
    bool set_value(ConfigId id, int32_t v);
    bool set_value(ConfigId id, uint32_t v);
    bool set_value(ConfigId id, int64_t v);
    bool set_value(ConfigId id, uint64_t v);
    bool set_value(ConfigId id, const std::string &v);
    bool set_value(ConfigId id, const std::vector<std::byte> v);
    bool set_value(ConfigId id, const void *out, size_t size);

    nvs_handle_t _handle = 0;
};
