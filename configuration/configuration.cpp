#include <configuration.h>
#include <stdlib.h>
#include <nvs_flash.h>
#include <esp_err.h>

static constexpr int keySize = 6;
using key_type               = char[keySize];

static void idToKey(ConfigId id, key_type buf)
{
    snprintf(buf, keySize, "%d", static_cast<int>(id));
}

Configuration::Configuration()
{
    ESP_ERROR_CHECK(nvs_open("Configuration", NVS_READWRITE, &_handle));
}

Configuration::~Configuration()
{
    nvs_close(_handle);
}

bool Configuration::get_value(ConfigId id, int8_t &out) const
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_get_i8(_handle, key, &out);
    return status == ESP_OK;
}

bool Configuration::get_value(ConfigId id, uint8_t &out) const
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_get_u8(_handle, key, &out);
    return status == ESP_OK;
}

bool Configuration::get_value(ConfigId id, int16_t &out) const
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_get_i16(_handle, key, &out);
    return status == ESP_OK;
}

bool Configuration::get_value(ConfigId id, uint16_t &out) const
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_get_u16(_handle, key, &out);
    return status == ESP_OK;
}

bool Configuration::get_value(ConfigId id, int32_t &out) const
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_get_i32(_handle, key, &out);
    return status == ESP_OK;
}

bool Configuration::get_value(ConfigId id, uint32_t &out) const
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_get_u32(_handle, key, &out);
    return status == ESP_OK;
}

bool Configuration::get_value(ConfigId id, int64_t &out) const
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_get_i64(_handle, key, &out);
    return status == ESP_OK;
}

bool Configuration::get_value(ConfigId id, uint64_t &out) const
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_get_u64(_handle, key, &out);
    return status == ESP_OK;
}

bool Configuration::get_value(ConfigId id, std::string &out) const
{
    key_type key;
    idToKey(id, key);
    size_t    size;
    esp_err_t status = nvs_get_str(_handle, key, nullptr, &size);
    if(status != ESP_OK)
        return false;
    out.resize(size);
    status = nvs_get_str(_handle, key, out.data(), &size);
    out.resize(size - 1);
    return status == ESP_OK;
}

bool Configuration::get_value(ConfigId id, std::vector<std::byte> &out) const
{
    key_type key;
    idToKey(id, key);
    size_t    size;
    esp_err_t status = nvs_get_blob(_handle, key, nullptr, &size);
    if(status != ESP_OK)
        return false;
    out.resize(size);
    status = nvs_get_blob(_handle, key, out.data(), &size);
    out.resize(size - 1);
    return status == ESP_OK;
}

bool Configuration::get_value(ConfigId id, void *out, size_t size) const
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_get_blob(_handle, key, out, &size);
    return status == ESP_OK;
}

bool Configuration::set_value(ConfigId id, int8_t v)
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_set_i8(_handle, key, v);
    if(status == ESP_OK)
        ESP_ERROR_CHECK(nvs_commit(_handle));
    return status == ESP_OK;
}

bool Configuration::set_value(ConfigId id, uint8_t v)
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_set_u8(_handle, key, v);
    if(status == ESP_OK)
        ESP_ERROR_CHECK(nvs_commit(_handle));
    return status == ESP_OK;
}

bool Configuration::set_value(ConfigId id, int16_t v)
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_set_i16(_handle, key, v);
    if(status == ESP_OK)
        ESP_ERROR_CHECK(nvs_commit(_handle));
    return status == ESP_OK;
}

bool Configuration::set_value(ConfigId id, uint16_t v)
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_set_u16(_handle, key, v);
    if(status == ESP_OK)
        ESP_ERROR_CHECK(nvs_commit(_handle));
    return status == ESP_OK;
}

bool Configuration::set_value(ConfigId id, int32_t v)
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_set_i32(_handle, key, v);
    if(status == ESP_OK)
        ESP_ERROR_CHECK(nvs_commit(_handle));
    return status == ESP_OK;
}

bool Configuration::set_value(ConfigId id, uint32_t v)
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_set_u32(_handle, key, v);
    if(status == ESP_OK)
        ESP_ERROR_CHECK(nvs_commit(_handle));
    return status == ESP_OK;
}

bool Configuration::set_value(ConfigId id, int64_t v)
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_set_i64(_handle, key, v);
    if(status == ESP_OK)
        ESP_ERROR_CHECK(nvs_commit(_handle));
    return status == ESP_OK;
}

bool Configuration::set_value(ConfigId id, uint64_t v)
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_set_u64(_handle, key, v);
    if(status == ESP_OK)
        ESP_ERROR_CHECK(nvs_commit(_handle));
    return status == ESP_OK;
}

bool Configuration::set_value(ConfigId id, const std::string &v)
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_set_str(_handle, key, v.c_str());
    if(status == ESP_OK)
        ESP_ERROR_CHECK(nvs_commit(_handle));
    return status == ESP_OK;
}

bool Configuration::set_value(ConfigId id, const std::vector<std::byte> v)
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_set_blob(_handle, key, v.data(), v.size());
    if(status == ESP_OK)
        ESP_ERROR_CHECK(nvs_commit(_handle));
    return status == ESP_OK;
}

bool Configuration::set_value(ConfigId id, const void *v, size_t size)
{
    key_type key;
    idToKey(id, key);
    esp_err_t status = nvs_set_blob(_handle, key, v, size);
    if(status == ESP_OK)
        ESP_ERROR_CHECK(nvs_commit(_handle));
    return status == ESP_OK;
}
