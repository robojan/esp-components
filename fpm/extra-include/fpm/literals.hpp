#pragma once

namespace fpm::literals
{
constexpr fpm::fixed_16_16 operator"" _fp16_16(long double x)
{
    return fpm::fixed_16_16{x};
}

constexpr fpm::fixed_24_8 operator"" _fp24_8(long double x)
{
    return fpm::fixed_24_8{x};
}

constexpr fpm::fixed_8_24 operator"" _fp8_24(long double x)
{
    return fpm::fixed_8_24{x};
}

constexpr fpm::fixed_16_16 operator"" _fp16_16(unsigned long long x)
{
    return fpm::fixed_16_16{x};
}

constexpr fpm::fixed_24_8 operator"" _fp24_8(unsigned long long x)
{
    return fpm::fixed_24_8{x};
}

constexpr fpm::fixed_8_24 operator"" _fp8_24(unsigned long long x)
{
    return fpm::fixed_8_24{x};
}

constexpr fpm::fixed_16_16 operator"" _fp16(long double x)
{
    return fpm::fixed_16_16{x};
}

constexpr fpm::fixed_24_8 operator"" _fp8(long double x)
{
    return fpm::fixed_24_8{x};
}

constexpr fpm::fixed_8_24 operator"" _fp24(long double x)
{
    return fpm::fixed_8_24{x};
}

constexpr fpm::fixed_16_16 operator"" _fp16(unsigned long long x)
{
    return fpm::fixed_16_16{x};
}

constexpr fpm::fixed_24_8 operator"" _fp8(unsigned long long x)
{
    return fpm::fixed_24_8{x};
}

constexpr fpm::fixed_8_24 operator"" _fp24(unsigned long long x)
{
    return fpm::fixed_8_24{x};
}

} // namespace fpm::literals