#pragma once

#include <configuration.h>
#include <memory>
#include <freertos/FreeRTOS.h>
#include <freertos/timers.h>
#include <sdkconfig.h>

class NetworkManager
{
    static constexpr int         MaxConnectionAttempts = CONFIG_NETWORKMANAGER_CONNECTION_ATTEMPTS;
    static constexpr int         ReconnectionTime      = CONFIG_NETWORKMANAGER_RECONNECT_TIME;
    static constexpr const char *MDNSInstanceName      = CONFIG_NETWORKMANAGER_MDNS_INSTANCE_NAME;

public:
    enum ConnectionStatus
    {
        Disconnected,
        Connecting,
        Connected,
    };

    NetworkManager(std::shared_ptr<Configuration> config);
    ~NetworkManager();

    void connect_wifi();

    void reloadSettings();

private:
    std::shared_ptr<Configuration> _config;
    wifi_mode_t                    _wifiMode          = WIFI_MODE_NULL;
    int                            _connectionTries   = 0;
    ConnectionStatus               _connectionStatus  = Disconnected;
    TimerHandle_t                  _reconnectionTimer = nullptr;
    esp_netif_t                   *_wifiStaIf         = nullptr;
    esp_netif_t                   *_wifiApIf          = nullptr;

    // General networking stuff
    void init_mdns();
    bool setHostName();

    // WiFi stuff
    void init_wifi();
    void deinit_wifi();
    void init_wifi_ap();
    void deinit_wifi_ap();
    void init_wifi_sta();
    void deinit_wifi_sta();
    void init_base_wifi();

    // Callbacks
    static void sWifiEventHandler(NetworkManager  *self,
                                  esp_event_base_t event_base,
                                  int32_t          event_id,
                                  void            *event_data);

    void        wifiEventHandler(esp_event_base_t event_base, int32_t event_id, void *event_data);
    static void sIpEventHandler(NetworkManager  *self,
                                esp_event_base_t event_base,
                                int32_t          event_id,
                                void            *event_data);

    void        ipEventHandler(esp_event_base_t event_base, int32_t event_id, void *event_data);
    static void sReconnectionTimerCallback(TimerHandle_t timer);
};
