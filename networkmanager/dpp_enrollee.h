// Copyright 2022 Robbert-Jan de Jager
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may
//    be used to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
/*****************************************************************************
 * Created on: 2022 February 12
 * Author: Robbert-Jan de Jager
 *
 * This file contains an implementation of the Device Provision protocol
 ****************************************************************************/
#pragma once

#include <Configuration.h>
#include <esp_dpp.h>

#include <memory>

class DppEnrollee
{
public:
    DppEnrollee(std::shared_ptr<Configuration> &&config);

private:
    std::shared_ptr<Configuration> _config;

    static void sDppCallback(esp_supp_dpp_event_t evt, void *data);
    static void sWifiEventHandler(DppEnrollee     *self,
                                  esp_event_base_t event_base,
                                  int32_t          event_id,
                                  void            *event_data);
    void        WifiEventHandler(DppEnrollee     *self,
                                 esp_event_base_t event_base,
                                 int32_t          event_id,
                                 void            *event_data);
    static void sDppEventHandler(DppEnrollee     *self,
                                 esp_event_base_t event_base,
                                 int32_t          event_id,
                                 void            *event_data);
    void        DppEventHandler(DppEnrollee     *self,
                                esp_event_base_t event_base,
                                int32_t          event_id,
                                void            *event_data);
};
