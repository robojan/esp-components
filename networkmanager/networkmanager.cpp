#include <networkmanager.h>

#include <esp_err.h>
#include <esp_log.h>
#include <cstring>
#include <mdns.h>

static const char *TAG = "NM";

NetworkManager::NetworkManager(std::shared_ptr<Configuration> config) : _config(std::move(config))
{
    _reconnectionTimer = xTimerCreate("ReconnectionTimer", pdMS_TO_TICKS(1000 * ReconnectionTime),
                                      pdFALSE, this, &NetworkManager::sReconnectionTimerCallback);
    assert(_reconnectionTimer != nullptr);

    ESP_ERROR_CHECK(esp_netif_init());

    // Register IP event callback
    ESP_ERROR_CHECK(esp_event_handler_instance_register(
        IP_EVENT, ESP_EVENT_ANY_ID,
        reinterpret_cast<esp_event_handler_t>(&NetworkManager::sIpEventHandler), this, nullptr));

    init_wifi();
    setHostName();
    ESP_ERROR_CHECK(esp_wifi_start());
    init_mdns();
}

NetworkManager::~NetworkManager()
{
    mdns_free();

    deinit_wifi();

    esp_netif_deinit();
}


void NetworkManager::connect_wifi()
{
    if(_connectionStatus != Disconnected)
    {
        ESP_LOGW(TAG, "Tried to connect while not disconnected");
        return;
    }

    _connectionStatus = Connecting;
    _connectionTries  = 0;
    esp_wifi_connect();
}

void NetworkManager::reloadSettings()
{
    ESP_LOGI(TAG, "Reloading network configuration");
    // Deinit current network
    mdns_free();
    deinit_wifi();

    // Reinit the network
    init_wifi();
    ESP_ERROR_CHECK(esp_wifi_start());
    init_mdns();
    setHostName();
}

bool NetworkManager::setHostName()
{
    auto hostname = _config->get_default<ConfigId::Hostname>();
    bool success  = true;
    if(hostname.size() >= 32)
    {
        ESP_LOGW(TAG, "Hostname is too long");
    }
    auto cStrHostname = hostname.c_str();
    ESP_LOGI(TAG, "Setting Hostname to: %s", cStrHostname);
    if(_wifiStaIf)
    {
        success &= esp_netif_set_hostname(_wifiStaIf, cStrHostname) == ESP_OK;
    }
    if(_wifiApIf)
    {
        success &= esp_netif_set_hostname(_wifiApIf, cStrHostname) == ESP_OK;
    }
    return success;
}

void NetworkManager::init_mdns()
{
    auto hostname = _config->get_default<ConfigId::Hostname>();
    ESP_ERROR_CHECK(mdns_init());
    ESP_ERROR_CHECK(mdns_hostname_set(hostname.c_str()));
    ESP_ERROR_CHECK(mdns_instance_name_set(MDNSInstanceName));
}

void NetworkManager::init_wifi()
{
    auto mode = _config->get_default<ConfigId::WifiMode>();
    switch(mode)
    {
    default:
        ESP_LOGW(TAG, "Unknown wifi mode(%d) defaulting to AP", mode);
        [[fallthrough]];
    case WIFI_MODE_AP:
        init_wifi_ap();
        break;
    case WIFI_MODE_STA:
        init_wifi_sta();
        break;
    }
}

void NetworkManager::deinit_wifi()
{
    switch(_wifiMode)
    {
    default:
        break;
    case WIFI_MODE_AP:
        deinit_wifi_ap();
        break;
    case WIFI_MODE_STA:
        init_wifi_ap();
        break;
    }
}

void NetworkManager::init_wifi_ap()
{
    ESP_LOGI(TAG, "Starting WiFi access point");
    _wifiMode = WIFI_MODE_AP;

    auto ssid       = _config->get_default<ConfigId::WifiSSID>();
    auto password   = _config->get_default<ConfigId::WifiPassword>();
    auto authMode   = _config->get_default<ConfigId::WifiAuthMode>();
    auto channel    = _config->get_default<ConfigId::WifiAPChannel>();
    auto hiddenSsid = _config->get_default<ConfigId::WifiAPHidden>();

    _wifiApIf = esp_netif_create_default_wifi_ap();

    init_base_wifi();

    wifi_config_t cfg = {};
    strncpy(reinterpret_cast<char *>(cfg.ap.ssid), ssid.c_str(), sizeof(cfg.ap.ssid) - 1);
    cfg.ap.ssid[sizeof(cfg.ap.ssid) - 1] = '\0';
    cfg.ap.ssid_len = ssid.size() > sizeof(cfg.ap.ssid) - 1 ? sizeof(cfg.ap.ssid) - 1 : ssid.size();
    strncpy(reinterpret_cast<char *>(cfg.ap.password), password.c_str(),
            sizeof(cfg.ap.password) - 1);
    cfg.ap.password[sizeof(cfg.ap.password) - 1] = '\0';
    cfg.ap.channel                               = channel;
    cfg.ap.authmode                              = authMode;
    cfg.ap.ssid_hidden                           = hiddenSsid ? 1 : 0;
    cfg.ap.max_connection                        = 1;
    cfg.ap.beacon_interval                       = 100;
    if(password.size() == 0)
    {
        ESP_LOGW(TAG, "Empty wifi password. Disabling authentication!");
        cfg.ap.authmode = WIFI_AUTH_OPEN;
    }
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &cfg));

    ESP_LOGI(TAG, "Wifi initialized. SSID: %s, Password: %s, channel: %d", ssid.c_str(),
             password.c_str(), channel);
}

void NetworkManager::deinit_wifi_ap()
{
    esp_wifi_stop();
    esp_netif_destroy(_wifiApIf);
    _wifiApIf = nullptr;
    ESP_LOGI(TAG, "Stopping WiFi access point");
}

void NetworkManager::init_wifi_sta()
{
    ESP_LOGI(TAG, "Starting WiFi station");
    _wifiMode = WIFI_MODE_STA;

    auto ssid     = _config->get_default<ConfigId::WifiSSID>();
    auto password = _config->get_default<ConfigId::WifiPassword>();
    auto authMode = _config->get_default<ConfigId::WifiAuthMode>();

    _wifiStaIf = esp_netif_create_default_wifi_sta();

    init_base_wifi();

    wifi_config_t cfg = {};
    strncpy(reinterpret_cast<char *>(cfg.sta.ssid), ssid.c_str(), sizeof(cfg.sta.ssid) - 1);
    cfg.sta.ssid[sizeof(cfg.sta.ssid) - 1] = '\0';
    strncpy(reinterpret_cast<char *>(cfg.sta.password), password.c_str(),
            sizeof(cfg.sta.password) - 1);
    cfg.sta.password[sizeof(cfg.sta.password) - 1] = '\0';
    cfg.sta.threshold.authmode                     = authMode;
    cfg.sta.pmf_cfg.capable                        = true;
    cfg.sta.pmf_cfg.required                       = false;

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &cfg));

    ESP_LOGI(TAG, "Wifi initialized. SSID: %s", ssid.c_str());
}

void NetworkManager::deinit_wifi_sta()
{
    esp_wifi_stop();
    esp_netif_destroy(_wifiStaIf);
    _wifiStaIf = nullptr;
    ESP_LOGI(TAG, "Stopping WiFi station");
}

void NetworkManager::init_base_wifi()
{
    wifi_init_config_t wifi_cfg = WIFI_INIT_CONFIG_DEFAULT();
    wifi_cfg.nvs_enable         = false;
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_cfg));

    ESP_ERROR_CHECK(esp_event_handler_instance_register(
        WIFI_EVENT, ESP_EVENT_ANY_ID,
        reinterpret_cast<esp_event_handler_t>(&NetworkManager::sWifiEventHandler), this, nullptr));
}

void NetworkManager::sWifiEventHandler(NetworkManager * self,
                                       esp_event_base_t event_base,
                                       int32_t          event_id,
                                       void *           event_data)
{
    self->wifiEventHandler(event_base, event_id, event_data);
}

void NetworkManager::wifiEventHandler(esp_event_base_t event_base,
                                      int32_t          event_id,
                                      void *           event_data)
{
    switch(event_id)
    {
    case WIFI_EVENT_AP_STACONNECTED:
    {
        wifi_event_ap_staconnected_t *event = (wifi_event_ap_staconnected_t *)event_data;
        _connectionStatus                   = Connected;
        ESP_LOGI(TAG, "station " MACSTR " join, AID=%d", MAC2STR(event->mac), event->aid);
        break;
    }
    case WIFI_EVENT_AP_STADISCONNECTED:
    {
        wifi_event_ap_stadisconnected_t *event = (wifi_event_ap_stadisconnected_t *)event_data;
        _connectionStatus                      = Disconnected;
        ESP_LOGI(TAG, "station " MACSTR " leave, AID=%d", MAC2STR(event->mac), event->aid);
        break;
    }
    case WIFI_EVENT_STA_CONNECTED:
    {
        _connectionStatus = Connected;
        break;
    }
    case WIFI_EVENT_STA_DISCONNECTED:
    {
        if(_connectionStatus == Connected)
        {
            // We were connected so try to reconnect
            ESP_LOGI(TAG, "Lost wifi connection");
            _connectionStatus = Disconnected;
            connect_wifi();
        }
        else if(_connectionStatus == Connecting)
        {
            // We were connecting. retry
            _connectionTries++;
            if(_connectionTries < MaxConnectionAttempts)
            {
                ESP_LOGW(TAG, "Failed to connect to access point. Retrying %d/%d...",
                         _connectionTries, MaxConnectionAttempts - 1);
                esp_wifi_connect();
            }
            else
            {
                ESP_LOGW(TAG, "Failed to connect to access point. Retry every %d seconds.",
                         ReconnectionTime);
                _connectionStatus = Disconnected;
                xTimerStart(_reconnectionTimer, portMAX_DELAY);
            }
        }
        else
        {
            ESP_LOGW(TAG, "Got disconnected event while we should not be connected");
        }
        break;
    }
    case WIFI_EVENT_STA_START:
    {
        connect_wifi();
        break;
    }
    default:
        break;
    }
}

void NetworkManager::sIpEventHandler(NetworkManager * self,
                                     esp_event_base_t event_base,
                                     int32_t          event_id,
                                     void *           event_data)
{
    self->ipEventHandler(event_base, event_id, event_data);
}

void NetworkManager::ipEventHandler(esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    if(event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
    }
}

void NetworkManager::sReconnectionTimerCallback(TimerHandle_t timer)
{
    auto self = reinterpret_cast<NetworkManager *>(pvTimerGetTimerID(timer));
    ESP_LOGI(TAG, "Trying to connect to access point");
    self->connect_wifi();
}