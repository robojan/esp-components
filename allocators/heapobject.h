#pragma once

#include <memory>
#include <cassert>

template <typename T, typename Alloc = std::allocator<T>>
class HeapObject
{
public:
    template <typename... Args>
    HeapObject(Args &&...args) : _ptr(nullptr)
    {
        using traits_t = std::allocator_traits<Alloc>;
        _ptr           = traits_t::allocate(_allocator, 1);
        assert(_ptr);
        if(!_ptr)
            return;
        traits_t::construct(_allocator, _ptr, std::forward<Args>(args)...);
    }

    HeapObject(const HeapObject &) = delete;

    HeapObject(HeapObject &&other) : _ptr(nullptr)
    {
        _allocator = std::exchange(other._allocator, {});
        _ptr       = std::exchange(other._ptr, nullptr);
    }

    ~HeapObject()
    {
        if(_ptr)
        {
            using traits_t = std::allocator_traits<Alloc>;
            traits_t::destroy(_allocator, _ptr);
            traits_t::deallocate(_allocator, _ptr, 1);
            _ptr = nullptr;
        }
    }

    HeapObject &operator=(const HeapObject &) = delete;
    HeapObject &operator                      =(HeapObject &&other)
    {
        _allocator = std::exchange(other._allocator, {});
        _ptr       = std::exchange(other._ptr, nullptr);
        return *this;
    }

         operator bool() const { return _ptr != nullptr; }
    bool isValid() const { return _ptr != nullptr; }

    T &      operator*() { return *_ptr; }
    const T &operator*() const { return *_ptr; }
    T *      operator->() { return _ptr; }
    const T *operator->() const { return _ptr; }

    T *      get() { return _ptr; }
    const T *get() const { return _ptr; }

private:
    Alloc _allocator;
    T *   _ptr;
};