#pragma once

#include <esp_heap_caps.h>

template <typename T, uint32_t Caps>
struct HeapCapsAllocator
{
    typedef T value_type;

    template <class U>
    struct rebind
    {
        typedef HeapCapsAllocator<U, Caps> other;
    };


    HeapCapsAllocator() = default;
    template <typename U>
    constexpr HeapCapsAllocator(const HeapCapsAllocator<U, Caps> &) noexcept {};

    [[nodiscard]] T *allocate(std::size_t n)
    {
        auto p = static_cast<T *>(heap_caps_malloc(n * sizeof(T), Caps));
        assert(p);
        return p;
    }

    void deallocate(T *p, std::size_t n) noexcept { heap_caps_free(p); }
};

template <typename T, uint32_t TCaps, typename U, uint32_t UCaps>
bool operator==(const HeapCapsAllocator<T, TCaps> &, const HeapCapsAllocator<U, UCaps> &)
{
    return TCaps == UCaps;
}

template <typename T, uint32_t TCaps, typename U, uint32_t UCaps>
bool operator!=(const HeapCapsAllocator<T, TCaps> &, const HeapCapsAllocator<U, UCaps> &)
{
    return TCaps != UCaps;
}


// Define standard capability allocators
template <typename T>
using DmaAllocator = HeapCapsAllocator<T, MALLOC_CAP_DMA>;

template <typename T>
using ExecAllocator = HeapCapsAllocator<T, MALLOC_CAP_EXEC>;

template <typename T>
using SpiRamAllocator = HeapCapsAllocator<T, MALLOC_CAP_SPIRAM>;

template <typename T>
using InternalAllocator = HeapCapsAllocator<T, MALLOC_CAP_INTERNAL>;

template <typename T>
using IRamAllocator = HeapCapsAllocator<T, MALLOC_CAP_IRAM_8BIT>;