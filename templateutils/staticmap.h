#pragma once

namespace staticmap::detail
{
template <auto A, auto B>
struct IsEqual
{
    static constexpr bool value = false;
};

template <typename T, T A, T B>
struct IsEqual<A, B>
{
    static constexpr bool value = A == B;
};

} // namespace staticmap::detail

template <auto K, typename V>
struct StaticMapEntry
{
    using KeyType                = decltype(K);
    using ValueType              = V;
    static constexpr KeyType Key = K;
    ValueType                value;
};

template <auto K, typename V>
constexpr StaticMapEntry<K, V> makeStaticMapEntry(V &&v)
{
    return StaticMapEntry<K, V>{std::forward<V>(v)};
}


template <typename StaticMapEntry, typename... Args>
class StaticMap
{
public:
    constexpr StaticMap(StaticMapEntry &&entry, Args &&...args)
        : _entry(std::forward<StaticMapEntry>(entry)), _tail(std::forward<Args>(args)...)
    {
    }

    template <auto K>
    const auto &get() const
    {
        if constexpr(staticmap::detail::IsEqual<K, StaticMapEntry::Key>::value)
            return _entry.value;
        else
            return _tail.template get<K>();
    }

    template <auto K>
    auto &get()
    {
        if constexpr(staticmap::detail::IsEqual<K, StaticMapEntry::Key>::value)
            return _entry.value;
        else
            return _tail.template get<K>();
    }

private:
    StaticMapEntry     _entry;
    StaticMap<Args...> _tail;
};


template <typename StaticMapEntry>
class StaticMap<StaticMapEntry>
{
public:
    constexpr StaticMap(StaticMapEntry &&entry) : _entry(std::forward<StaticMapEntry>(entry)) {}

    template <auto K>
    const auto &get() const
    {
        if constexpr(staticmap::detail::IsEqual<K, StaticMapEntry::Key>::value)
            return _entry.value;
        else
            throw std::range_error("Not found");
    }

    template <auto K>
    auto &get()
    {
        if constexpr(staticmap::detail::IsEqual<K, StaticMapEntry::Key>::value)
            return _entry.value;
        else
            throw std::range_error("Not found");
    }

private:
    StaticMapEntry _entry;
};
