#pragma once
#include <cassert>
#include <type_traits>

/** Read from a volatile variable
 *
 * @tparam TType the type of the variable. This will be deduced by the compiler.
 * @note TType shall satisfy the requirements of TrivallyCopyable.
 * @param target The pointer to the volatile variable to read from.
 * @returns the value of the volatile variable.
 */
template <typename TType>
constexpr inline TType volatile_load(const TType *target)
{
    assert(target);
    static_assert(std::is_trivially_copyable<TType>::value,
                  "Volatile load can only be used with trivially copiable types");
    return *static_cast<const volatile TType *>(target);
}

/** Write to a volatile variable
 *
 * Causes the value of `*target` to be overwritten with `value`.
 *
 * @tparam TType the type of the variable. This will be deduced by the compiler.
 * @note TType shall satisfy the requirements of TrivallyCopyable.
 * @param target The pointer to the volatile variable to update.
 * @param value The new value for the volatile variable.
 */
template <typename TType>
inline void volatile_store(TType *target, TType value)
{
    assert(target);
    static_assert(std::is_trivially_copyable<TType>::value,
                  "Volatile store can only be used with trivially copiable types");
    *static_cast<volatile TType *>(target) = value;
}

template <typename TType>
class volatile_variable
{
    static_assert(std::is_trivially_copyable<TType>::value,
                  "Volatile variable can only be used with trivially copiable types");

    TType _val;

public:
    volatile_variable(TType v) : _val(v) {}

    void  operator=(TType v) { volatile_store(&_val, v); }
          operator TType() const { return volatile_load(&_val); }
    TType load() const { return volatile_load(&_val); }
    void  store(TType v) const { volatile_store(&_val, v); }
};