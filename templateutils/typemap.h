#pragma once

template <auto I, typename T>
struct ct_pair
{
    using type                        = T;
    using value_type                  = decltype(I);
    static constexpr value_type value = I;

    static ct_pair get_pair(std::integral_constant<value_type, value>) { return {}; }
};

template <typename I_T, typename... Pairs>
struct type_map : public Pairs...
{
    using Pairs::get_pair...;

    template <I_T I>
    using get_type = typename decltype(get_pair(std::integral_constant<I_T, I>{}))::type;
};