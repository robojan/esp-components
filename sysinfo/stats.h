#pragma once

#include <vector>
#include <string>
#include <fpm/fixed.hpp>
#include <fpm/math.hpp>
#include <fpm/literals.hpp>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <atomic>

struct TaskRunTimeStats
{
    std::string name;
    uint32_t    elapsedTimeUs = 0;
    uint16_t    unusedStack   = 0;
    uint8_t     load          = 0;
};

struct RunTimeStats
{
    bool                          valid            = false;
    uint32_t                      totalElapsedTime = 0;
    uint8_t                       load             = 0;
    std::vector<TaskRunTimeStats> taskStats;
};

RunTimeStats getRuntimeStats(TickType_t duration = 100);


class StatsService
{
    enum TaskState
    {
        Starting,
        Running,
        Stopping,
        Stopped,
    };

public:
    StatsService(TickType_t period, bool printStats = false);
    ~StatsService();

    void printStats();

private:
    void task();

    TickType_t             _period;
    RunTimeStats           _stats;
    bool                   _printStats;
    std::atomic<TaskState> _taskState{Starting};
    TaskHandle_t           _taskHandle;
    portMUX_TYPE           _spinlock = portMUX_INITIALIZER_UNLOCKED;
};
