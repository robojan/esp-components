#pragma once

#include <etl/string.h>

namespace hwinfo
{
static constexpr int cMaxSerialNumberLength = 16;

etl::string<cMaxSerialNumberLength> getSerialNumber();

} // namespace hwinfo