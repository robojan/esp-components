#include "stats.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <priorities.h>
#include <esp_log.h>

static const char *TAG = "Stats";

static std::pair<std::vector<TaskStatus_t>, uint32_t> getSystemStatus()
{
    auto                      numTasks = 0;
    std::vector<TaskStatus_t> state;

    uint32_t totalRunTime;
    // Try to get the run time stats, taking into account that more tasks can be created while
    // executing this code.
    do
    {
        numTasks = uxTaskGetNumberOfTasks();
        state.resize(numTasks);
        numTasks = uxTaskGetSystemState(state.data(), state.size(), &totalRunTime);
    } while(numTasks == 0);

    return {state, totalRunTime};
}

RunTimeStats getRuntimeStats(TickType_t duration)
{
    using namespace fpm::literals;
    auto [preState, preRunTime] = getSystemStatus();

    // Wait a certain time to get the delta over the time period
    vTaskDelay(duration);

    auto [postState, postRuntime] = getSystemStatus();

    // Calculate the elapsed time.
    auto elapsedTime = postRuntime - preRunTime;
    if(elapsedTime == 0)
    {
        return {};
    }

    // Prepare the result
    RunTimeStats result;
    result.valid            = true;
    result.totalElapsedTime = elapsedTime;
    result.taskStats.reserve(std::min(preState.size(), postState.size()));


    // For every task get the statistics.
    for(auto &preTaskState : preState)
    {
        // Find the matching end task
        auto endTaskStateIt = std::find_if(postState.begin(), postState.end(),
                                           [&preTaskState](TaskStatus_t state) {
                                               return state.xTaskNumber == preTaskState.xTaskNumber;
                                           });
        if(endTaskStateIt == postState.end())
        {
            // End task not found
            continue;
        }

        TaskRunTimeStats taskStats{preTaskState.pcTaskName};

        auto taskElapsedTime    = endTaskStateIt->ulRunTimeCounter - preTaskState.ulRunTimeCounter;
        taskStats.elapsedTimeUs = taskElapsedTime;
        uint32_t totalCpuTime   = (elapsedTime * portNUM_PROCESSORS);
        taskStats.load          = (taskElapsedTime * 100UL + totalCpuTime / 2) / totalCpuTime;
        taskStats.unusedStack   = endTaskStateIt->usStackHighWaterMark;

        // Check if the current task is idle.
        bool isIdle = false;
        for(int i = 0; i < portNUM_PROCESSORS; i++)
        {
            if(xTaskGetIdleTaskHandleForCPU(i) == preTaskState.xHandle)
            {
                isIdle = true;
                break;
            }
        }

        // Store the task result
        if(!isIdle)
        {
            result.load += taskStats.load;
        }

        result.taskStats.push_back(std::move(taskStats));
    }

    return result;
}


StatsService::StatsService(TickType_t period, bool printStats)
    : _period(period), _printStats(printStats)
{
    // Create the task
    auto result = xTaskCreate([](void *self) { reinterpret_cast<StatsService *>(self)->task(); },
                              "Stats", 2048, this, priorities::task::stats, &_taskHandle);
    assert(result == pdTRUE);
}

StatsService::~StatsService()
{
    // Stop the task
    taskENTER_CRITICAL(&_spinlock);
    if(_taskState != Stopped)
    {
        _taskState = Stopping;
    }
    taskEXIT_CRITICAL(&_spinlock);

    // Wait until it has stopped
    while(_taskState != Stopped)
    {
        vTaskDelay(1);
    }
}

void StatsService::printStats()
{
    if(_stats.valid)
    {
        ESP_LOGI(TAG, "CPU load: %d%%", _stats.load);
        ESP_LOGI(TAG, "Name:             CPU   Stack");
        for(auto &task : _stats.taskStats)
        {
            ESP_LOGI(TAG, "%16s: %3d%% %5d", task.name.c_str(), task.load, task.unusedStack);
        }
    }
    else
    {
        ESP_LOGW(TAG, "No valid stats loaded");
    }
}

void StatsService::task()
{
    _taskState = Running;

    while(_taskState == Running)
    {
        auto stats = getRuntimeStats(_period);
        taskENTER_CRITICAL(&_spinlock);
        _stats = std::move(stats);
        taskEXIT_CRITICAL(&_spinlock);

        // Print the statistics if enabled
        if(_printStats)
        {
            printStats();
        }
    }

    taskENTER_CRITICAL(&_spinlock);
    _taskState = Stopped;
    taskEXIT_CRITICAL(&_spinlock);
}
