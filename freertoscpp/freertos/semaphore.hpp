#pragma once

#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <chrono>

namespace freertos
{
class BinarySemaphore
{
public:
    inline BinarySemaphore() { _handle = xSemaphoreCreateBinaryStatic(&_buf); }
    inline ~BinarySemaphore() { vSemaphoreDelete(_handle); }

    inline void release(std::ptrdiff_t update = 1)
    {
        while(update--)
        {
            xSemaphoreGive(_handle);
        }
    }

    inline void acquire() noexcept { xSemaphoreTake(_handle, portMAX_DELAY); }

    inline bool try_acquire() noexcept { return xSemaphoreTake(_handle, 0) == pdTRUE; }

    template <class Rep, class Period>
    bool try_acquire_for(const std::chrono::duration<Rep, Period> &rel_time)
    {
        auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(rel_time).count();
        if(ms < 0)
            ms = 0;
        auto ticks = (ms * configTICK_RATE_HZ + 999) / 1000; // Round up
        return xSemaphoreTake(_handle, ticks) == pdTRUE;
    }

    template <class Clock, class Duration>
    bool try_acquire_until(const std::chrono::time_point<Clock, Duration> &abs_time)
    {
        auto duration = abs_time - Clock::now();
        return try_lock_for(duration);
    }

    static constexpr std::ptrdiff_t max() noexcept { return 1; }

protected:
    StaticSemaphore_t _buf;
    SemaphoreHandle_t _handle;
};

template <std::ptrdiff_t Max>
class Semaphore
{
public:
    inline Semaphore(std::ptrdiff_t initial = 0)
    {
        _handle = xSemaphoreCreateCountingStatic(Max, initial, &_buf);
    }

    inline ~Semaphore() { vSemaphoreDelete(_handle); }

    inline void release(std::ptrdiff_t update = 1)
    {
        while(update--)
        {
            xSemaphoreGive(_handle);
        }
    }

    inline void acquire() noexcept { xSemaphoreTake(_handle, portMAX_DELAY); }

    inline bool try_acquire() noexcept { return xSemaphoreTake(_handle, 0) == pdTRUE; }

    template <class Rep, class Period>
    bool try_acquire_for(const std::chrono::duration<Rep, Period> &rel_time)
    {
        auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(rel_time).count();
        if(ms < 0)
            ms = 0;
        auto ticks = (ms * configTICK_RATE_HZ + 999) / 1000; // Round up
        return xSemaphoreTake(_handle, ticks) == pdTRUE;
    }

    template <class Clock, class Duration>
    bool try_acquire_until(const std::chrono::time_point<Clock, Duration> &abs_time)
    {
        auto duration = abs_time - Clock::now();
        return try_lock_for(duration);
    }

    static constexpr std::ptrdiff_t max() noexcept { return Max; }

protected:
    StaticSemaphore_t _buf;
    SemaphoreHandle_t _handle;
};


} // namespace freertos