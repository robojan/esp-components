#pragma once

#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <chrono>

namespace freertos
{
class Mutex
{
    StaticSemaphore_t _buffer;
    SemaphoreHandle_t _mutex = nullptr;

public:
    inline Mutex() { _mutex = xSemaphoreCreateMutexStatic(&_buffer); }
    inline ~Mutex() { vSemaphoreDelete(_mutex); }

    inline void lock() { xSemaphoreTake(_mutex, portMAX_DELAY); }

    inline bool try_lock() { return xSemaphoreTake(_mutex, 0) == pdTRUE; }

    template <class Rep, class Period>
    bool try_lock_for(const std::chrono::duration<Rep, Period> &timeout_duration)
    {
        auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(timeout_duration).count();
        if(ms < 0)
            ms = 0;
        auto ticks = (ms * configTICK_RATE_HZ + 999) / 1000; // Round up
        return xSemaphoreTake(_mutex, ticks) == pdTRUE;
    }

    template <class Clock, class Duration>
    bool try_lock_until(const std::chrono::time_point<Clock, Duration> &timeout_time)
    {
        auto duration = timeout_time - Clock::now();
        return try_lock_for(duration);
    }

    inline void unlock() { xSemaphoreGive(_mutex); }
};

} // namespace freertos
