# Register the component
idf_component_register(INCLUDE_DIRS "etl-20.17.0/include")

# Create and import the library targets
add_subdirectory(etl-20.17.0)

if(CONFIG_ETL_ARRAY_VIEW_IS_MUTABLE)
    target_compile_definitions(${COMPONENT_TARGET} INTERFACE ETL_ARRAY_VIEW_IS_MUTABLE)
endif()