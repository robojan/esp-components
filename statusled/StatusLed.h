#pragma once

#include <cstdint>
#include <driver/rmt.h>
#include <array>
#include <algorithm>

namespace drivers
{
namespace detail
{
static constexpr int apb_clk_freq = 80000000;

namespace ws2812
{
static constexpr int t0h_ns   = 350;
static constexpr int t0l_ns   = 1000;
static constexpr int t1h_ns   = 1000;
static constexpr int t1l_ns   = 350;
static constexpr int reset_us = 200;

}; // namespace ws2812

constexpr int ns_to_rmt_ticks(int ns, int freq)
{
    float numTicks = static_cast<float>(ns) * freq / 1e9f;
    assert(numTicks >= 1 || numTicks < INT16_MAX);
    return static_cast<int>(numTicks);
}
}; // namespace detail

class StatusLed
{
    static constexpr int counterClkDiv = 2;
    static constexpr int rmtClkFreq    = detail::apb_clk_freq / counterClkDiv;
    static constexpr int t0h_ticks = detail::ns_to_rmt_ticks(detail::ws2812::t0h_ns, rmtClkFreq);
    static constexpr int t0l_ticks = detail::ns_to_rmt_ticks(detail::ws2812::t0l_ns, rmtClkFreq);
    static constexpr int t1h_ticks = detail::ns_to_rmt_ticks(detail::ws2812::t1h_ns, rmtClkFreq);
    static constexpr int t1l_ticks = detail::ns_to_rmt_ticks(detail::ws2812::t1l_ns, rmtClkFreq);
    static constexpr int reset_ticks =
        detail::ns_to_rmt_ticks(detail::ws2812::reset_us * 1000, rmtClkFreq);

    struct ws2812_val
    {
        uint32_t val;

        explicit ws2812_val(uint8_t r, uint8_t g, uint8_t b) : val((r << 8) | (g << 0) | (b << 16))
        {
        }

        ws2812_val operator*=(float gain)
        {
            auto r  = ((val >> 8) & 0xff) * gain;
            auto g  = ((val >> 0) & 0xff) * gain;
            auto b  = ((val >> 16) & 0xff) * gain;
            auto rb = static_cast<uint8_t>(std::clamp(r, 0.0f, 255.0f));
            auto gb = static_cast<uint8_t>(std::clamp(g, 0.0f, 255.0f));
            auto bb = static_cast<uint8_t>(std::clamp(b, 0.0f, 255.0f));
            val     = (rb << 8) | (gb << 0) | (bb << 16);
            return *this;
        }
    };

public:
    enum EventCode
    {
        Off,
        Good,
        Warning,
        Error,
        Event1,
        Event2,
        Event3,
        Event4,
    };

    StatusLed(gpio_num_t port, rmt_channel_t channel);
    ~StatusLed();

    void setValue(EventCode code)
    {
        switch(code)
        {
        case StatusLed::Off:
            setValue(0, 0, 0);
            break;
        case StatusLed::Good:
            setValue(0, 0xff, 0);
            break;
        default:
        case StatusLed::Warning:
            setValue(0xbb, 0xff, 0);
            break;
        case StatusLed::Error:
            setValue(0xff, 0, 0);
            break;
        case StatusLed::Event1:
            setValue(0, 0, 0xff);
            break;
        case StatusLed::Event2:
            setValue(0, 0xff, 0xaa);
            break;
        case StatusLed::Event3:
            setValue(0xff, 0, 0xff);
            break;
        case StatusLed::Event4:
            setValue(0xff, 0xff, 0xff);
            break;
        }
    }
    void setValue(ws2812_val rgb);
    void setValue(uint8_t r, uint8_t g, uint8_t b) { setValue(ws2812_val{r, g, b}); }

    float gain() const { return _gain; }
    void  setGain(float gain) { _gain = gain; }

private:
    void configureDriver(gpio_num_t port, rmt_channel_t channel);

    rmt_channel_t                _channel;
    std::array<rmt_item32_t, 25> _sendBuffer;
    float                        _gain;
};

} // namespace drivers