#include <StatusLed.h>

using namespace drivers;


StatusLed::StatusLed(gpio_num_t port, rmt_channel_t channel) : _channel(channel)
{
    configureDriver(port, channel);
}

StatusLed::~StatusLed()
{
    rmt_driver_uninstall(_channel);
}

void StatusLed::setValue(ws2812_val rgb)
{
    ESP_ERROR_CHECK(rmt_wait_tx_done(_channel, portMAX_DELAY));

    rgb *= _gain;

    // Send the data bits
    for(int i = 23; i >= 0; i--)
    {
        auto &item  = _sendBuffer[i];
        auto  isSet = (rgb.val & (1 << i)) != 0;
        item.level0 = 1;
        item.level1 = 0;
        if(isSet)
        {
            item.duration0 = t1h_ticks;
            item.duration1 = t1l_ticks;
        }
        else
        {
            item.duration0 = t0h_ticks;
            item.duration1 = t0l_ticks;
        }
    }
    // The reset command
    _sendBuffer[24].level0    = 0;
    _sendBuffer[24].duration0 = reset_ticks;

    // Send the data 24 bits + reset
    ESP_ERROR_CHECK(rmt_write_items(_channel, _sendBuffer.data(), 24 * 2 + 1, false));
}

void StatusLed::configureDriver(gpio_num_t port, rmt_channel_t channel)
{
    rmt_config_t config{};
    config.rmt_mode                 = RMT_MODE_TX;
    config.channel                  = channel;
    config.gpio_num                 = port;
    config.clk_div                  = counterClkDiv;
    config.mem_block_num            = 1;
    config.flags                    = 0;
    config.tx_config.idle_level     = RMT_IDLE_LEVEL_LOW;
    config.tx_config.carrier_en     = false;
    config.tx_config.loop_en        = false;
    config.tx_config.idle_output_en = true;

    ESP_ERROR_CHECK(rmt_config(&config));
    ESP_ERROR_CHECK(rmt_driver_install(config.channel, 0, 0));
}